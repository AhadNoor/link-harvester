## Application Requirements
* PHP Version:  It's recommended to use a more recent version of PHP for security and performance reasons.

* NodeJS version: 16+
* Composer: Composer is a dependency management tool for PHP and is used extensively in Laravel for managing packages and dependencies. Ensure you have Composer installed.

* Database Extensions: Laravel supports multiple database systems. You'll need the appropriate PHP extensions for your chosen database. For example:

    * MySQL: mysqli or pdo_mysql
    * PostgreSQL: pgsql or pdo_pgsql
    * SQLite: pdo_sqlite
    * OpenSSL Extension: Laravel uses OpenSSL for various security-related tasks. Ensure the openssl extension is enabled in your PHP configuration.

* Mbstring Extension: The mbstring extension is used for multibyte character encoding, which can be essential for working with internationalization and text processing.

* JSON Extension: Laravel makes extensive use of JSON for configuration and data interchange. Ensure the json extension is enabled.

* Tokenizer Extension: The tokenizer extension is used for parsing PHP code and is required by Composer for autoloading classes.

* XML Extension: The xml extension is used for working with XML data, which can be useful in various scenarios.

* Curl Extension (Optional): While not strictly required, the curl extension is often used for making HTTP requests to external APIs and services.

* Fileinfo Extension: The fileinfo extension is used for determining the MIME type of files, which can be useful when handling file uploads.

## Running the application
* set proper DB connection .env
* ___composer install___
* ___php artisan migrate___
* ___npm install___
* ___npm run build___
* ___make sure appropriate value for REDIS_HOST, REDIS_PASSWORD, REDIS_PORT on .env___
* set QUEUE_CONNECTION=redis on .env
* ___php artisan horizon___

## To Do

* docker-compose configurations have some problem and still under in progress
