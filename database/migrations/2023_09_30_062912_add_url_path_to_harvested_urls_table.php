<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('harvested_urls', function (Blueprint $table) {
            $table->string('url_path')->after('base_domain_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('harvested_urls', function (Blueprint $table) {
            $table->dropColumn('url_path');
        });
    }
};
