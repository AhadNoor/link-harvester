import Alpine from "alpinejs";

Alpine.data("formdata", () => ({
    fields: {
        urls: {
            value: null,
            validate() {
                this.invalidURLs = [];
                this.isValid = this.value && this.value.trim().length > 0;
                this.errorMsg = 'URLs is required!';
                this.items = this.value.split('\n');
                if (!this.isValid) {
                    return false;
                }
                this.items.forEach((line) => {
                    // Remove leading and trailing whitespace from each line
                    const trimmedLine = line.trim();
                    if(!trimmedLine || trimmedLine.length <= 0){
                        return;
                    }

                    // Use a regular expression to validate as a URL (you can adjust this regex as needed)
                    const urlRegex = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/;

                    if (!urlRegex.test(trimmedLine)) {
                        this.invalidURLs.push(trimmedLine)
                    }
                });
                this.isValid = this.invalidURLs.length == 0;
                this.errorMsg = 'There are some invalid URLs. Please see the list above.';

            },
            isValid: true,
            errorMsg: null,
            invalidURLs: [],
            items: []
        },

    },
    isFormValid: true,
    invalidURLs: [
        1,2
    ],
    submit_url: {
        value: null
    },
    isFormSubmittedSuccessfully: false,
    submit() {
        this.isFormValid = !Object.values(this.fields).some(
            (field) => !field.isValid
        );

        if(! this.isFormValid) {
            return false;
        }
        this.submitFormData();
    },
    submitFormData() {
        const formSubmitURLElement = document.getElementById('form-submit-url');
        const csrfTokenElement = document.getElementById('csrf-token');

        const formData = new FormData();
        formData.append('urls', JSON.stringify(this.fields.urls.items));
        formData.append('_token', csrfTokenElement.value);

        fetch(formSubmitURLElement.value, {
            method: 'POST',
            body: formData,

        })
            .then(response => response.json())
            .then(data => {
                const redirectingURLElement = document.getElementById('redirecting-url');
                this.isFormSubmittedSuccessfully =true;
                setTimeout(() => {
                    window.location = redirectingURLElement.value;
                }, 2000);
            })
            .catch(error => {
                // Handle errors here
                console.error('Error:', error);
            });
    }

}));


Alpine.start();
