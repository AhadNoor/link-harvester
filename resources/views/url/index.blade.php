@extends('layout.app')

@section('content')
    <div class="container mx-auto px-4">
        <h1 class="inline-block text-xl-left sm:text-xl font-weight-bold text-slate-900 tracking-tight dark:text-slate-200 mb-4">Harvest URL List</h1>

        {{-- Search Form --}}
        <form action="{{ route('url.list') }}" method="GET" class="mb-4">
            <div class="flex">
                <input type="text" name="search_url" placeholder="Enter URL" class="w-full px-3 mr-2 py-2 rounded-l-md focus:outline-none focus:ring focus:border-blue-300" value="{{ request('search') }}">
                <input type="text" name="search_domain" placeholder="Enter Domain Name" class="w-full mr-2 px-3 py-2 rounded-l-md focus:outline-none focus:ring focus:border-blue-300" value="{{ request('search') }}">
                <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded-r-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-300">Search</button>
            </div>
        </form>

        <table class="min-w-full divide-y divide-gray-200">
            <thead>
            <tr>
                <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                    <a href="{{ route('url.list', ['sort' => 'harvested_base_domains.domain', 'order' => $sort === 'harvested_base_domains.domain' ? $order === 'asc' ? 'desc' : 'asc' : 'asc']) }}">
                        Domain
                        @if ($sort === 'harvested_base_domains.domain')
                            @if ($order === 'asc')
                                <i class="fas fa-sort-up"></i>
                            @else
                                <i class="fas fa-sort-down"></i>
                            @endif
                        @endif
                    </a>
                </th>
                <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                    <a href="{{ route('url.list', ['sort' => 'harvested_urls.url', 'order' => $sort === 'harvested_urls.url' ? $order === 'asc' ? 'desc' : 'asc' : 'asc']) }}">
                        URL
                        @if ($sort === 'harvested_urls.url')
                            @if ($order === 'asc')
                                <i class="fas fa-sort-up"></i>
                            @else
                                <i class="fas fa-sort-down"></i>
                            @endif
                        @endif
                    </a>
                </th>

            </tr>
            </thead>
            <tbody>
            @foreach ($items as $item)
                <tr>
                    <td class="px-6 py-4 whitespace-no-wrap">{{ $item->domain }}</td>
                    <td class="px-6 py-4 whitespace-no-wrap">{{ $item->url }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{-- Pagination Links --}}
        <div class="mt-4">
            {{ $items->appends(request()->input())->links() }}
        </div>
    </div>
@endsection

