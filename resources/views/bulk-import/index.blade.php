@extends('layout.app')
@section('content')

    <div class="relative rounded-xl overflow-auto">
        <div class="shadow-sm overflow-hidden ">
            <h1 class="inline-block text-xl-left sm:text-xl font-weight-bold text-slate-900 tracking-tight dark:text-slate-200 mb-4">
                Bulk Import Listing
            </h1>


            {{-- Bulk Import List --}}
            <<table class="min-w-full divide-y divide-gray-200">
                <thead>
                <tr>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        <a>Action</a>
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        Uploaded File
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        <a href="{{ route('bulk.import.index', ['sort' => 'is_processed', 'order' => $order == 'asc' ? 'desc' : 'asc']) }}">Status</a>
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        Total
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        Success
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        Failure
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        Invalid
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">
                        <a style="color: #1f4480" class="dark:text-blue-500"
                           href="{{ route('bulk.import.index', ['sort' => 'created_at', 'order' => $order == 'asc' ? 'desc' : 'asc']) }}">
                            Created At
                            @if ($sort === 'created_at')
                                @if ($order === 'asc')
                                    <i class="fas fa-sort-up"></i>
                                @else
                                    <i class="fas fa-sort-down"></i>
                                @endif
                            @endif
                        </a>
                    </th>
                    <th class="px-6 py-3 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-600 uppercase tracking-wider">Action</th>
                </tr>
                </thead>
                <tbody class="align-baseline">
                @foreach ($bulkImports as $bulkImport)
                    <tr>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->getActionLabel() }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->metadata && $bulkImport->metadata['original_name'] ? $bulkImport->metadata['original_name'] : ''  }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->is_processed ? 'Completed' : 'Pending'  }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->total_count }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->success_count }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->failure_count }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->invalid_count }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">{{ $bulkImport->created_at->format('m-d-Y H:i:s') }}</td>
                        <td class="px-6 py-4 whitespace-no-wrap">
                            @if($bulkImport->upload_file_path)
                                <a href="{{ url('storage/'.$bulkImport->upload_file_path) }}"
                                   class="text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-1 text-center mr-2 mb-2 dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-500 dark:focus:ring-blue-800">
                                    <i class="la la-download"></i> Source
                                </a>
                            @endif

                            @if($bulkImport->result_file_path)
                                <a href="{{  url('storage/'.$bulkImport->result_file_path) }}"
                                   class="text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-1 text-center mr-2 mb-2 dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-500 dark:focus:ring-blue-800">
                                    <i class="la la-download"></i> Results
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{-- Pagination Links --}}
            {{ $bulkImports->appends(request()->input())->links() }}
        </div>
    </div>

@endsection
