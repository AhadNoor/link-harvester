<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel Link Harvester</title>
    <meta name="description" content="Laravel Link Harvester">
    <meta name="keywords" content="laravel link harvester, ahad noor alam, ahad">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.2.19/dist/tailwind.min.css"/>
    <!--Replace with your tailwind.css once created-->
    @vite('resources/css/app.css')
    @vite('resources/js/app.js')


</head>

<body class="bg-gray-100 font-sans leading-normal tracking-normal">

<nav id="header" class="bg-white fixed w-full z-10 top-0 shadow">
    <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 md:pb-0">
        <div class="w-full">
            <a class="text-gray-900 text-base xl:text-xl no-underline hover:no-underline font-bold px-4" href="/">
                <i class="fas fa-sun text-pink-600 pr-3"></i> Link Harvester
            </a>
        </div>

        @include('layout.inc.main-nav')

    </div>
</nav>

<!--Container-->
<div class="w-full  pt-24 bg-white px-6 py-6">
    <div class="w-full container mx-auto flex flex-wrap items-center mt-0  md:pb-0 pt-6 ">
        @yield('content')
    </div>
</div>
<!--/container-->

<footer class="bg-white border-t border-gray-400 shadow  bottom-2 w-full">
    <div class="container max-w-md mx-auto flex py-4">
        <div class="w-full mx-auto flex flex-wrap">
            <div class="flex w-full ">
                <div class="text-center mx-auto">
                    <p class="py-2 text-gray-600 text-sm">
                        Handcrafted by
                        <a href="https://www.linkedin.com/in/ahadnoor/" target="_blank" class="text-blue-900">
                            Ahad Noor Alam
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>


</body>

</html>
