<div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-white z-20 px-3"
     id="nav-content">
    <ul class="list-reset lg:flex flex-1 items-center px-4 md:px-0 ">
        <li class="mr-6 my-2 md:my-0">
            <a href="{{ route('url.list') }}"
               class="block py-1 md:py-3 pl-1 align-middle text-pink-600 no-underline hover:text-gray-900 border-b-2 {{ request()->routeIs('url.list') ? 'border-orange-600 hover:border-orange-600' : 'border-white hover:border-pink-500' }}">
                <i class="fas fa-home fa-fw mr-3 text-pink-600"></i><span
                    class="pb-1 md:pb-0 text-sm">Show URLs</span>
            </a>
        </li>
        <li class="mr-6 my-2 md:my-0">
            <a href="{{ route('url.create') }}"
               class="block py-1 md:py-3 pl-1 align-middle text-gray-500 no-underline hover:text-gray-900 border-b-2  {{ request()->routeIs('url.create') ? 'border-orange-600 hover:border-orange-600' : 'border-white hover:border-pink-500' }}">
                <i class="fas fa-link fa-fw mr-3"></i><span class="pb-1 md:pb-0 text-sm">Add URLs</span>
            </a>
        </li>

        <li class="mr-6 my-2 md:my-0">
            <a href="{{ route('bulk.import.index') }}"
               class="block py-1 md:py-3 pl-1 align-middle text-gray-500 no-underline hover:text-gray-900 border-b-2  {{ request()->routeIs('bulk.import.index') ? 'border-orange-600 hover:border-orange-600' : 'border-white hover:border-pink-500' }}">
                <i class="fas fa-tasks fa-fw mr-3"></i><span class="pb-1 md:pb-0 text-sm">Bulk Imports</span>
            </a>
        </li>

    </ul>

</div>
