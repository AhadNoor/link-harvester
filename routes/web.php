<?php

use App\Http\Controllers\BulkImportCrudController;
use App\Http\Controllers\UrlCrudController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(\route('url.list'));
});
Route::resource('urls', UrlCrudController::class)
    ->name('index', 'url.list')
    ->name('create', 'url.create')
    ->name('store', 'url.store');

Route::get('/bulk-imports', [BulkImportCrudController::class, 'index'])->name('bulk.import.index');
