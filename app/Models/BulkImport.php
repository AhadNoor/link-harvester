<?php declare(strict_types=1);

namespace App\Models;

use App\Enums\BulkImportAction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string|null $description Short description about uploaded file.
 * @property string $upload_file_path Uploaded file path
 * @property string|null $result_file_path Upload result file path generated after processing uploaded file.
 * @property string $action Bulk upload action ( Like: update basic info, salary structure, etc.).
 * @property int $is_processed Flag for file is processed or not.
 * @property int $total_count Total rows of uploaded file.
 * @property int $success_count Total success rows of uploaded file.
 * @property int $failure_count Total failure rows of uploaded file.
 * @property int $invalid_count Total invalid rows of uploaded file.
 * @property string|null $metadata Uploaded file meta data ( like: mime, size, name etc.)
 * @property int $uploaded_by
 * @property-read \App\Models\User $uploadedBy
 * @property-read \Illuminate\Database\Eloquent\Collection<int, BulkImportResult> $results Bulk Upload results
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class BulkImport extends Model
{
    use HasFactory;

    protected $table = 'bulk_imports';


    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'upload_file_path',
        'result_file_path',
        'action',
        'is_processed',
        'total_count',
        'success_count',
        'failure_count',
        'invalid_count',
        'metadata',
        'uploaded_by',
    ];

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'is_processed' => 'integer',
        'total_count' => 'integer',
        'success_count' => 'integer',
        'failure_count' => 'integer',
        'invalid_count' => 'integer',
        'metadata' => 'array',
    ];


    public function setUploadFilePathAttribute($value): void
    {
        $attributeName = "upload_file_path";
        $disk = "public";
        $destinationPath = 'bulk-uploads/'.date('Y-m-d');

        $file = request()->file('upload_file_path');

        $this->metadata = [
            'original_name' => $file->getClientOriginalName(),
            'extension' => $file->getClientOriginalExtension(),
            'mime_type' => $file->getClientMimeType(),
        ];

        $this->uploadFileToDisk($value, $attributeName, $disk, $destinationPath);
    }

    public function results(): HasMany
    {
        return $this->hasMany(BulkImportResult::class);
    }

    public function getActionLabel()
    {
        return BulkImportAction::from($this->action)->label();
    }

}
