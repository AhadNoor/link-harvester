<?php declare(strict_types=1);

namespace App\Models;

use App\Enums\BulkImportResultType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $bulk_import_id Associated bulk import ID.
 * @property string $subject Subject.
 * @property string $result Row Result as json
 * @property string|null $details Details information about uploaded file's row if needed.
 * @property \App\Models\BulkImport $bulkUpdate Associated BulkUpdate Model.
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class BulkImportResult extends Model
{
    use HasFactory;

    protected $table = 'bulk_import_results';

    /** @var array<int, string> */
    protected $fillable = [
        'bulk_import_id',
        'subject',
        'result',
        'details',
    ];

    protected $casts = [
        'result' => BulkImportResultType::class,
    ];


    public function bulkImport(): BelongsTo
    {
        return $this->belongsTo(BulkImport::class);
    }
}
