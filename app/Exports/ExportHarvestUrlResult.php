<?php declare(strict_types=1);

namespace App\Exports;

use App\Models\BulkImportResult;
use App\Models\BulkUpdateResult;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportHarvestUrlResult extends ExportBulkImportResult
{
    protected function getHeadings(): array
    {
        return[
            'URL',
            'Result',
            'Details',
        ];
    }

}
