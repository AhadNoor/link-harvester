<?php declare(strict_types=1);

namespace App\Exports;

use App\Models\BulkImportResult;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportBulkImportResult implements FromCollection, WithHeadings
{
    public function __construct(public int $bulkImportId)
    {
    }

    protected function getHeadings(): array
    {
        return[
            'Subject',
            'Result',
            'Details',
        ];
    }

    public function headings(): array
    {
        return $this->getHeadings();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection(): Collection
    {
        return BulkImportResult::query()->select('subject', 'result', 'details')->where('bulk_import_id', $this->bulkImportId)->get();
    }
}
