<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\BulkImport;
use Illuminate\Http\Request;


class BulkImportCrudController extends Controller
{

    public function index(Request $request)
    {
        $query = BulkImport::query();

        // Sorting logic
        $sort = $request->input('sort', 'created_at');
        $order = $request->input('order', 'desc');
        $query->orderBy($sort, $order);

        // Pagination
        $bulkImports = $query->paginate(10);

        return view('bulk-import.index', compact('bulkImports', 'order', 'sort'));
    }

}
