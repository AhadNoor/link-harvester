<?php

namespace App\Http\Controllers;

use App\Enums\BulkImportAction;
use App\Jobs\BulkImport\ProcessBulkImport;
use App\Models\BulkImport;
use App\Models\HarvestedUrl;
use Illuminate\Http\Request;

class UrlCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $sort = $request->input('sort', 'harvested_urls.id');
        $order = $request->input('order', 'desc');
        $searchURL = $request->input('search_url');
        $searchDomain = $request->input('search_domain');

        $query = HarvestedUrl::query()
            ->join('harvested_base_domains', 'harvested_urls.base_domain_id', '=', 'harvested_base_domains.id');

        // Apply search filter if a search term is provided
        if ($searchURL) {
            $query->where('harvested_urls.url', 'like', "%$searchURL%");
        }

        if ($searchDomain) {
            $query->where('harvested_base_domains.domain', 'like', "%$searchDomain%");
        }

        // Apply sorting
        $query->orderBy($sort, $order);

        $items = $query->paginate(10); // Change the number per page as needed

        return view('url.index', compact('items', 'sort', 'order'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('url.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $urls = $request->get('urls');
        if (is_string($urls)) {
            $urls = json_decode(trim($urls));
        }
        $processingRows = [];
        foreach ($urls as $url) {
            if (empty(trim($url))) {
                continue;
            }
            $processingRows[] = ['subject' => trim($url)];
        }

        $bulkImport = new BulkImport();
        $bulkImport->action = BulkImportAction::HARVEST_URL_FROM_ARRAY;
        $bulkImport->total_count = count($processingRows);
        $bulkImport->save();

        ProcessBulkImport::dispatch($bulkImport, $processingRows);
        return ['success' => true];
    }

    /**
     * Display the specified resource.
     */
    public function show(HarvestedUrl $harvestedUrl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(HarvestedUrl $harvestedUrl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, HarvestedUrl $harvestedUrl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(HarvestedUrl $harvestedUrl)
    {
        //
    }
}
