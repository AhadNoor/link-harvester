<?php declare(strict_types=1);

namespace App\Imports\BulkImports;

use App\Jobs\BulkImport\UpdateBulkImportResult;
use App\Models\BulkImport;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

abstract class AbstractBulkImport implements WithHeadingRow
{
    use Importable;

    protected BulkImport $bulkImport;
    protected array $processRowJobs = [];

    protected array $processingRows = [];


    /** Processes each row of the bulk update file */
    abstract protected function processRow(array $row): void;

    public function setProcessingRows(array $processingRows = []): void
    {
        $this->processingRows = $processingRows;
    }

    private function getProcessingRows(): array
    {
        if (!empty($this->processingRows)) {
            return $this->processingRows;
        }
        $rows = $this->toArray(Storage::disk('public')->path($this->bulkImport->upload_file_path));
        return $rows[0] ?? [];
    }

    public function process(BulkImport $bulkImport): void
    {
        $this->bulkImport = $bulkImport;
        $processingRows = $this->getProcessingRows();
        foreach ($processingRows as $row) {
            $this->processRow($row);
        }

        if (empty($this->processRowJobs)) {
            $this->processRowJobs[] = new UpdateBulkImportResult($this->bulkImport->id);
        }

        $bulkUpdateId = $this->bulkImport->id;
        $dispatchUpdateResult = !empty($this->processRowJobs);

        Bus::batch($this->processRowJobs)
            ->allowFailures()
            ->finally(function () use ($bulkUpdateId, $dispatchUpdateResult) {
                if ($dispatchUpdateResult === false) {
                    return;
                }
                UpdateBulkImportResult::dispatch($bulkUpdateId);
                $this->processRowJobs = [];
            })->dispatch();

    }


}
