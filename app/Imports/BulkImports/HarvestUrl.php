<?php

declare(strict_types=1);

namespace App\Imports\BulkImports;

use App\Jobs\BulkImport\ProcessLinkHarvesting;

class HarvestUrl extends AbstractBulkImport
{
    protected function processRow(array $row): void
    {
        $this->processRowJobs[] = new ProcessLinkHarvesting($row, $this->bulkImport->id);
    }
}
