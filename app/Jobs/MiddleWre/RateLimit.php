<?php

namespace App\Jobs\MiddleWre;

use Illuminate\Support\Facades\Redis;

class RateLimit
{

    /**
     * @var mixed
     */
    protected $jobUniqueKey;

    /**
     * Create class instance.
     *
     * @param mixed $key
     * @return void
     */
    public function __construct($key)
    {
        $this->jobUniqueKey = $key;
    }

    /**
     * Process the queued job.
     *
     * @param mixed $job
     * @param callable $next
     * @return mixed
     */
    public function handle($job, $next)
    {
        Redis::throttle($this->jobUniqueKey)
            ->block(20)->allow(60)->every(100)
            ->then(function () use ($job, $next) {
                $next($job);
            }, function () use ($job) {
                $job->release(5);
            });
    }

}
