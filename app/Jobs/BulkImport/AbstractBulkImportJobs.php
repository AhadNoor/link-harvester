<?php

namespace App\Jobs\BulkImport;

use App\Enums\BulkImportResultType;
use App\Jobs\MiddleWre\RateLimit;
use App\Models\BulkImportResult;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

abstract class AbstractBulkImportJobs  implements ShouldQueue
{
    use Batchable;
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected array $row = [];
    protected int  $bulkImportId = 0;
    abstract protected function executeProcessedRow(array $row): void;

    abstract protected function getValidator(array $row): Validator;
    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (!$this->validateRow($this->row)) {
            return;
        }
        $bulkUpdateResult = new BulkImportResult();
        $bulkUpdateResult->subject = $this->row['subject'] ?? '';
        $bulkUpdateResult->bulk_import_id = $this->bulkImportId;
        DB::beginTransaction();
        try {
            $this->executeProcessedRow($this->row);
            DB::commit();
            $bulkUpdateResult->result = BulkImportResultType::SUCCESS;
        } catch (\Exception $exception){
            DB::rollBack();
            $bulkUpdateResult->result = BulkImportResultType::FAILED;
            $bulkUpdateResult->details =  $exception->getMessage();
        }
        $bulkUpdateResult->save();
    }

    /**
     * @return array
     */
    public function middleware(): array
    {
        if(empty($this->row['subject']))
        {
            return [];
        }
        $strKey = sprintf('%s-%d',$this->row['subject'], $this->bulkImportId );

        return [new RateLimit( $strKey), (new WithoutOverlapping($strKey))->dontRelease()];
    }

    public function validateRow($row): bool
    {
        $validator = $this->getValidator($row);

        $data = [
            'subject' => $row['subject'] ?? '',
            'bulk_import_id' => $this->bulkImportId
        ];

        if ($validator->fails()) {
            $errorMessages = [];

            foreach ($validator->errors()->messages() as $error) {
                $errorMessages[] = join(' ', $error);
            }

            $data['result'] = BulkImportResultType::INVALID;
            $data['details'] = join('', $errorMessages);
            BulkImportResult::create($data);
            return false;
        }
        return true;
    }
}
