<?php

namespace App\Jobs\BulkImport;

use App\Exports\ExportHarvestUrlResult;
use App\Models\BulkImport;
use App\Models\BulkImportResult;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class UpdateBulkImportResult implements ShouldQueue
{
    use Batchable;
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(private int $bulkImportId)
    {

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $bulkImportResult = BulkImportResult::query()
            ->where('bulk_import_id', '=', $this->bulkImportId)
            ->select('result', DB::raw('count(id) as total' ))
            ->groupBy('result')
            ->pluck('total', 'result')
            ->toArray();

        $resultPath = '/bulk-uploads/results/'.$this->bulkImportId.'_'.Str::uuid().'.csv';

        BulkImport::query()
            ->where('id', '=', $this->bulkImportId)
            ->update(
                [
                    'result_file_path' => $resultPath,
                    'total_count' => array_sum($bulkImportResult),
                    'success_count' => $bulkImportResult['success'] ?? 0,
                    'failure_count' => $bulkImportResult['failed'] ?? 0,
                    'invalid_count' => $bulkImportResult['invalid'] ?? 0,
                    'is_processed' => 1
                ]
            );

        Excel::store(new ExportHarvestUrlResult($this->bulkImportId), $resultPath, 'public');
    }

}
