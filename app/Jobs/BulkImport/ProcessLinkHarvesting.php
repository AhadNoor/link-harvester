<?php declare(strict_types=1);

namespace App\Jobs\BulkImport;

use App\Models\HarvestedBaseDomain;
use App\Models\HarvestedUrl;
use Illuminate\Support\Facades\Validator;

class ProcessLinkHarvesting extends AbstractBulkImportJobs
{

    public function __construct(array $row, int $bulkImportId)
    {
        $this->row = $row;
        $this->bulkImportId = $bulkImportId;
    }


    protected function executeProcessedRow(array $row): void
    {
        $parsedUrl = parse_url($row['subject']);
        if ($parsedUrl === false) {
            throw  new \Exception(sprintf('%s is not a valid URL ', $row['subject']));
        }

        $existingDomain = HarvestedBaseDomain::query()
            ->where('domain', '=', $parsedUrl['host']);

        if (!$existingDomain->exists()) {
            logger()->info('NE.' . $parsedUrl['host']);
            $domainId = HarvestedBaseDomain::query()->insertGetId([
                'domain' => $parsedUrl['host'],
            ]);
        } else {
            $existingDomainRow = $existingDomain->first();
            $domainId = $existingDomainRow->id;
            logger()->info('Exist.' . $domainId . '-' . $parsedUrl['host']);
        }

        HarvestedUrl::create([
            'base_domain_id' => $domainId,
            'url' => $row['subject'],
            'url_path' => $parsedUrl['path'],
        ]);

    }

    protected function getValidator(array $row): \Illuminate\Validation\Validator
    {
        return Validator::make($row, [
            'subject' => ['required', 'url', 'unique:harvested_urls,url'],
        ]);
    }


}
