<?php declare(strict_types=1);

namespace App\Jobs\BulkImport;

use App\Enums\BulkImportAction;
use App\Imports\BulkImports\HarvestUrl;
use App\Models\BulkImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

final class ProcessBulkImport implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public BulkImport $bulkImport, private array $processingRows = [])
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {

        $importerClass = match (BulkImportAction::from($this->bulkImport->action)) {
            BulkImportAction::HARVEST_URL_FROM_ARRAY => HarvestUrl::class,
        };

        /** @var \App\Imports\BulkImports\AbstractBulkImport $importer */
        $importer = app()->make($importerClass);
        if (!empty($this->processingRows)) {
            $importer->setProcessingRows($this->processingRows);
        }

        $importer->process($this->bulkImport);
    }

    public function failed(\Throwable $exception): void
    {
        $this->bulkImport->metadata = array_merge($this->bulkImport->metadata, [
            'failed' => $exception->getMessage(),
            'failed_at' => $exception->getFile() . ':' . $exception->getLine(),
            // 'failed_trace' => $exception->getTraceAsString(),
        ]);
        $this->bulkImport->is_processed = true;

        $this->bulkImport->save();
    }
}
