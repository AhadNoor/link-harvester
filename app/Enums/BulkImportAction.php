<?php declare(strict_types=1);

namespace App\Enums;

enum BulkImportAction: string
{
    use EnumOptions;

    case HARVEST_URL_FROM_ARRAY = 'harvest_url_from_array';


    public function label(): string
    {
        return match ($this) {
            self::HARVEST_URL_FROM_ARRAY => 'Harvest URL',
        };
    }
}
