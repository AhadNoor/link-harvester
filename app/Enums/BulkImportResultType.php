<?php declare(strict_types=1);

namespace App\Enums;

enum BulkImportResultType: string
{
    use EnumOptions;

    case SUCCESS = 'success';
    case FAILED = 'failed';
    case INVALID = 'invalid';

    public function label(): string
    {
        return match ($this) {
            self::SUCCESS => 'Success',
            self::FAILED => 'Failed',
            self::INVALID => 'Invalid',
        };
    }
}
